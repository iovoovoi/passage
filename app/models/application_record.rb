class ApplicationRecord < ActiveRecord::Base
  validates :phone, format: { with: /\A04\d{8}\z/ }
  self.abstract_class = true
end
