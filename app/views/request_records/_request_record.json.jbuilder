json.extract! request_record, :id, :firstName, :lastName, :location, :request_type, :brand, :model, :phone, :created_at, :updated_at
json.url request_record_url(request_record, format: :json)
