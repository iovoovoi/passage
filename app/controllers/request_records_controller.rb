require 'csv'
class RequestRecordsController < ApplicationController
  before_action :set_request_record, only: [:show, :edit, :update, :destroy]

  # GET /request_records
  # GET /request_records.json
  def index
    csv_text = File.read(Rails.root + "brand.csv")
    csv = CSV.parse(csv_text, :headers => true)
    csv.each do |row|
      puts "__________"
      puts row
      puts row.values_at(0).class
      puts row.values_at(1).first.class
    end
    @request_records = RequestRecord.all
  end

  # GET /request_records/1
  # GET /request_records/1.json
  def show
  end

  # GET /request_records/new
  def new
    @request_record = RequestRecord.new
  end

  # GET /request_records/1/edit
  def edit
  end

  def my_success

  end

  def passage
    @request_record = RequestRecord.new
  end

  def passage_second
    @request_record = RequestRecord.new
  end

  def passage_bmw
    @request_record = RequestRecord.new
  end

  def passage_audi
    @request_record = RequestRecord.new
  end

  def passage_benz_25102018
    @request_record = RequestRecord.new
  end

  def passage_toyota_25102018
    @request_record = RequestRecord.new
  end


  # POST /request_records
  # POST /request_records.json
  def create
      @request_record = RequestRecord.new
      @request_record.firstName = params[:firstName]
      @request_record.lastName = params[:lastName]
      if params[:location] == "1"
        @request_record.location = "悉尼"
      else
        @request_record.location = "墨尔本"
      end

      brand = params[:brand]
      model = params[:model]
      csv_text = File.read(Rails.root + "brand.csv")
      csv = CSV.parse(csv_text, :headers => true)
      csv.each do |row|
        if row.values_at(0).first == brand
          @request_record.brand = row.values_at(1).first
        end

        if row.values_at(0).first == model
          @request_record.model = row.values_at(1).first
        end
      end

      @request_record.phone = "0400000000"

      if params[:request_type] == "0"
        @request_record.request_type = "询问优惠"
      else
        @request_record.request_type = "预约试驾"
      end

      if @request_record.save
        redirect_to edit_request_record_path(@request_record.id)
      end
  end

  # PATCH/PUT /request_records/1
  # PATCH/PUT /request_records/1.json
  def update
    respond_to do |format|
      if @request_record.update(request_record_params)
        format.html { redirect_to my_success_path, notice: 'Request record was successfully updated.' }
        # format.json { render :show, status: :ok, location: @request_record }
      else
        format.html { render :edit }
        format.json { render json: @request_record.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /request_records/1
  # DELETE /request_records/1.json
  def destroy
    @request_record.destroy
    respond_to do |format|
      format.html { redirect_to request_records_url, notice: 'Request record was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_request_record
      @request_record = RequestRecord.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def request_record_params
      params.require(:request_record).permit(:firstName, :lastName, :location, :request_type, :brand, :model, :phone)
    end
end
