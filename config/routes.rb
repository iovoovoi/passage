Rails.application.routes.draw do
  resources :request_records
  get 'static_pages/index'

  post 'request_records/passage/audi', to: 'request_records#create'
  get 'request_records/passage/audi', to: 'request_records#passage_audi'

  post 'request_records/passage/bmw2', to: 'request_records#create'
  get 'request_records/passage/bmw2', to: 'request_records#passage_bmw'

  post 'request_records/passage/bmw', to: 'request_records#create'
  get 'request_records/passage/bmw', to: 'request_records#passage_second'

  post 'request_records/passage/levante', to: 'request_records#create'
  get 'request_records/passage/levante', to: 'request_records#passage'

  post 'request_records/passage/benz_25102018', to: 'request_records#create'
  get 'request_records/passage/benz_25102018', to: 'request_records#passage_benz_25102018'

  post 'request_records/passage/toyota_25102018', to: 'request_records#create'
  get 'request_records/passage/toyota_25102018', to: 'request_records#passage_toyota_25102018'

  get 'fihiauhefouygaouywefiasafawefsduaf', to: 'request_records#my_success', as: "my_success"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'static_pages#index'
end
