class CreateRequestRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :request_records do |t|
      t.string :firstName
      t.string :lastName
      t.string :location
      t.string :request_type
      t.string :brand
      t.string :model
      t.string :phone

      t.timestamps
    end
  end
end
