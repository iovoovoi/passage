require 'test_helper'

class RequestRecordsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @request_record = request_records(:one)
  end

  test "should get index" do
    get request_records_url
    assert_response :success
  end

  test "should get new" do
    get new_request_record_url
    assert_response :success
  end

  test "should create request_record" do
    assert_difference('RequestRecord.count') do
      post request_records_url, params: { request_record: { brand: @request_record.brand, firstName: @request_record.firstName, lastName: @request_record.lastName, location: @request_record.location, model: @request_record.model, phone: @request_record.phone, request_type: @request_record.request_type } }
    end

    assert_redirected_to request_record_url(RequestRecord.last)
  end

  test "should show request_record" do
    get request_record_url(@request_record)
    assert_response :success
  end

  test "should get edit" do
    get edit_request_record_url(@request_record)
    assert_response :success
  end

  test "should update request_record" do
    patch request_record_url(@request_record), params: { request_record: { brand: @request_record.brand, firstName: @request_record.firstName, lastName: @request_record.lastName, location: @request_record.location, model: @request_record.model, phone: @request_record.phone, request_type: @request_record.request_type } }
    assert_redirected_to request_record_url(@request_record)
  end

  test "should destroy request_record" do
    assert_difference('RequestRecord.count', -1) do
      delete request_record_url(@request_record)
    end

    assert_redirected_to request_records_url
  end
end
