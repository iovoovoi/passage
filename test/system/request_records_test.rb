require "application_system_test_case"

class RequestRecordsTest < ApplicationSystemTestCase
  setup do
    @request_record = request_records(:one)
  end

  test "visiting the index" do
    visit request_records_url
    assert_selector "h1", text: "Request Records"
  end

  test "creating a Request record" do
    visit request_records_url
    click_on "New Request Record"

    fill_in "Brand", with: @request_record.brand
    fill_in "Firstname", with: @request_record.firstName
    fill_in "Lastname", with: @request_record.lastName
    fill_in "Location", with: @request_record.location
    fill_in "Model", with: @request_record.model
    fill_in "Phone", with: @request_record.phone
    fill_in "Request Type", with: @request_record.request_type
    click_on "Create Request record"

    assert_text "Request record was successfully created"
    click_on "Back"
  end

  test "updating a Request record" do
    visit request_records_url
    click_on "Edit", match: :first

    fill_in "Brand", with: @request_record.brand
    fill_in "Firstname", with: @request_record.firstName
    fill_in "Lastname", with: @request_record.lastName
    fill_in "Location", with: @request_record.location
    fill_in "Model", with: @request_record.model
    fill_in "Phone", with: @request_record.phone
    fill_in "Request Type", with: @request_record.request_type
    click_on "Update Request record"

    assert_text "Request record was successfully updated"
    click_on "Back"
  end

  test "destroying a Request record" do
    visit request_records_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Request record was successfully destroyed"
  end
end
